<?php
/**
 * We Donuts - mu-plugin
 *
 * PHP version 8
 *
 * @category  Wordpress_Mu-plugin
 * @package   WeDonuts
 * @author    Colla <ola@agenciacolla.com.br>
 * @copyright 2023 Colla
 * @license   Proprietary https://agenciacolla.com.br
 * @link      https://wedonuts.com.br
 *
 * @wordpress-plugin
 * Plugin Name: We Donuts - mu-plugin
 * Plugin URI:  https://wedonuts.com.br
 * Description: Customizations for wedonuts.com.br site
 * Version:     3.0.4
 * Author:      Colla
 * Author URI:  https://agenciacolla.com.br/
 * Text Domain: wedonuts
 * License:     Proprietary
 * License URI: https://agenciacolla.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('wedonutscombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Hide editor
 */
add_action(
    'admin_head',
    function () {
        $template_file = $template_file = basename(get_page_template());

        $templates = array("page-politica-de-privacidade.blade.php");
        if (!in_array($template_file, $templates)) { 
            remove_post_type_support('page', 'editor');
        }
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

/**
 * Get a list of posts
 *
 * Generic function to return an array of posts formatted for CMB2. Simply pass
 * in your WP_Query arguments and get back a beautifully formatted CMB2 options
 * array.
 *
 * @param array $query_args WP_Query arguments
 * @return array CMB2 options array
 */
function Get_Post_array($query_args = array(), $exclude_current_post = false, $add_none = false) 
{
    $lang = function_exists('pll_current_language') && isset($_GET['post']) ? pll_get_post_language($_GET['post']) : '';
    $exclude_id = $exclude_current_post && isset($_GET['post']) ? array($_GET['post']) : '';

    $defaults = array(
        'posts_per_page' => -1,
        'orderby'          => 'title',
        'order'            => 'ASC',
        'post_status'      => 'publish',
        'post__not_in'     => $exclude_id,
        'lang'             => $lang
    );
    $query = new WP_Query(array_replace_recursive($defaults, $query_args));
    
    if ($add_none) {
        return array("-1" => '') + wp_list_pluck($query->get_posts(), 'post_title', 'ID');
    } else {
        return wp_list_pluck($query->get_posts(), 'post_title', 'ID');
    }
    
}

/***********************************************************************************
 * CMB2 Callback Functions
 **********************************************************************************/
/**
 * Only return default value if we don't have a post ID (in the 'post' query variable)
 *
 * @param  bool  $default On/Off (true/false)
 * @return mixed          Returns true or '', the blank default
 */
function cmb2_set_checkbox_default_for_new_post()
{
    return isset($_GET['post'])
        // No default value.
        ? ''
        // Default to true.
        : true;
}

/**********
 * Show on Front Page
 *
 * @return bool $display
 */
function Show_On_Front_page($cmb)
{
    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option('page_on_front');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($front_page) : $front_page;

    // There is a front page set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**********
 * Show on Home / Blog
 *
 * @return bool $display
 */
function Show_On_Home($cmb)
{
    // Get ID of page set as home, 0 if there isn't one
    $home = get_option('page_for_posts');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($home) : $home;

    // There is a home set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**********
 * Show on Privacy Policy 
 *
 * @return bool $display
 */
function Show_On_Privacy($cmb)
{
    // Get ID of page set as privacy, 0 if there isn't one
    $privacy = get_option('wp_page_for_privacy_policy');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($privacy) : $privacy;

    // There is a privacy set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**
 * Metabox for Page Slug
 *
 * @param bool  $display  Display
 * @param array $meta_box Metabox 
 *
 * @return bool $display
 *
 * @author Tom Morton
 * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
 */
function Cmb2_Metabox_Show_On_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_slug', 10, 2);

/**
 * Gets a number of terms and displays them as options
 *
 * @param CMB2_Field $field CMB2 Field
 *
 * @return array An array of options that matches the CMB2 options array
 */
function Cmb2_getTermOptions($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/**********
 * Stores
 *********/
function cpt_wedonuts_store() 
{
    $labels = array(
        'name'                  => __('Stores', 'wedonutscombr'),
        'singular_name'         => __('Store', 'wedonutscombr'),
        'menu_name'             => __('Stores', 'wedonutscombr'),
        'name_admin_bar'        => __('Store', 'wedonutscombr'),
        'archives'              => __('Store Archives', 'wedonutscombr'),
        'attributes'            => __('Store Attributes', 'wedonutscombr'),
        'parent_item_colon'     => __('Parent Store:', 'wedonutscombr'),
        'all_items'             => __('All Stores', 'wedonutscombr'),
        'add_new_item'          => __('Add New Store', 'wedonutscombr'),
        'add_new'               => __('Add New', 'wedonutscombr'),
        'new_item'              => __('New Store', 'wedonutscombr'),
        'edit_item'             => __('Edit Store', 'wedonutscombr'),
        'update_item'           => __('Update Store', 'wedonutscombr'),
        'view_item'             => __('View Store', 'wedonutscombr'),
        'view_items'            => __('View Stores', 'wedonutscombr'),
        'search_items'          => __('Search Store', 'wedonutscombr'),
        'not_found'             => __('Not found', 'wedonutscombr'),
        'not_found_in_trash'    => __('Not found in Trash', 'wedonutscombr'),
        'featured_image'        => __('Featured Image', 'wedonutscombr'),
        'set_featured_image'    => __('Set featured image', 'wedonutscombr'),
        'remove_featured_image' => __('Remove featured image', 'wedonutscombr'),
        'use_featured_image'    => __('Use as featured image', 'wedonutscombr'),
        'insert_into_item'      => __('Insert into case', 'wedonutscombr'),
        'uploaded_to_this_item' => __('Uploaded to this case', 'wedonutscombr'),
        'items_list'            => __('Stores list', 'wedonutscombr'),
        'items_list_navigation' => __('Stores list navigation', 'wedonutscombr'),
        'filter_items_list'     => __('Filter cases list', 'wedonutscombr'),
    );
    $rewrite = array(
        'slug'                  => 'unidades',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __('Store', 'wedonutscombr'),
        'desc'           => __('We Donuts Stores', 'wedonutscombr'),
        'labels'                => $labels,
        'supports'              => array('title', 'thumbnail', 'revisions', 'page-attributes'),
        'taxonomies'            => array(''),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6"><path d="M5.003 2.283a2.127 2.127 0 0 0-.598.197c-.317.153-.395.218-1.136.952-1.071 1.061-1.32 1.402-1.572 2.153-.136.406-.178.67-.181 1.162-.007 1.038.394 1.972 1.161 2.706L3 9.761v10.468l-.455.02c-.376.017-.483.036-.618.112-.216.121-.304.305-.306.639-.001.332.101.542.322.657l.157.083h19.8l.157-.083c.221-.115.323-.325.322-.657-.002-.334-.09-.518-.306-.639-.135-.076-.242-.095-.618-.112l-.455-.02v-5.255c0-2.973.015-5.254.035-5.254.053 0 .582-.551.762-.795.664-.896.874-2.21.525-3.28-.263-.802-.5-1.133-1.589-2.209-.734-.726-.818-.797-1.133-.949a1.992 1.992 0 0 0-.653-.206c-.38-.05-13.556-.048-13.944.002m13.949 1.553c.081.043.482.416.89.83.781.794.945 1.023 1.082 1.514.087.311.071.906-.034 1.242-.156.503-.548.997-.978 1.233-.379.207-.409.22-.65.283-.728.19-1.608-.078-2.162-.657-.206-.216-.413-.321-.63-.321-.199 0-.28.048-.613.363-.524.496-.921.657-1.617.655-.374-.001-.508-.018-.715-.092a2.254 2.254 0 0 1-.85-.539c-.268-.275-.458-.387-.658-.387-.254 0-.336.044-.689.375-.354.331-.675.515-1.058.605-.752.177-1.506-.032-2.083-.577-.341-.323-.484-.403-.719-.403-.19 0-.378.111-.608.359-.262.282-.752.549-1.172.637-.414.086-.983.028-1.288-.133-.408-.216-.525-.293-.712-.476a2.306 2.306 0 0 1-.571-.907c-.12-.351-.131-.998-.022-1.32.171-.504.294-.671 1.063-1.453.408-.415.805-.788.882-.829.135-.072.377-.075 6.952-.076 6.63-.002 6.815 0 6.96.074M8.04 10.077c1.183.606 2.624.543 3.706-.161l.246-.16.231.148c.862.551 2.02.734 2.991.471.305-.082.84-.326 1.083-.493l.182-.125.331.201c.355.216.743.367 1.202.467.306.066 1.015.087 1.298.038l.17-.03V20.24h-.718l-.011-3.53-.011-3.53-.093-.23a1.726 1.726 0 0 0-.673-.758c-.338-.183-.539-.199-2.374-.185l-1.7.013-.26.123c-.32.152-.619.443-.757.737l-.103.22-.011 3.57-.011 3.57H4.52V10.433l.15.021c.418.06.625.066.935.028.643-.08 1.186-.261 1.622-.542l.272-.176.121.08c.066.044.255.148.42.233m-1.665 1.978a1.574 1.574 0 0 0-1.023.897l-.092.228v2.18c0 1.986.006 2.198.071 2.38a1.6 1.6 0 0 0 .929.929c.182.065.394.071 2.38.071h2.18l.228-.092a1.73 1.73 0 0 0 .733-.627c.221-.376.223-.397.21-2.73l-.011-2.151-.112-.24a1.698 1.698 0 0 0-.764-.767l-.244-.113-2.14-.008c-1.614-.005-2.19.005-2.345.043m4.105 3.325v1.86H6.76v-3.72h3.72v1.86m6.76 1.5v3.36h-3v-6.72h3v3.36" fill-rule="evenodd" stroke="none"/></svg>'),
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => false,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'post',
    );
    register_post_type('wedonuts_store', $args);

}
add_action('init', 'cpt_wedonuts_store', 0);
/***********************************************************************************
 * CPT Fields
 * ********************************************************************************/

/******* 
 * Single Store
 ********/
function cmb_wedonuts_store() 
{
    $prefix = '_wedonuts_store_';
    $object_types = array('wedonuts_store');

    /**
     * Hero
     */
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => $prefix . 'hero_id',
            'title'         => __('Hero', 'wedonutscombr'),
            'object_types'  => $object_types, // post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );
            
    //Hero Show
    $cmb_hero->add_field(
        array(
            'name'       => __('Show Hero', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'show_hero',
            'type'       => 'checkbox',
            'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
        )
    );

    //Hero Autoplay
    $cmb_hero->add_field(
        array(
            'name'       => __('Autoplay Hero', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'hero_autoplay',
            'type'       => 'checkbox',
            'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
        )
    );

    //Hero Group
    $hero_id = $cmb_hero->add_field(
        array(
            'id'          => $prefix . 'hero_slides',
            'type'        => 'group',
            'desc' => '',
            'options'     => array(
                'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Hero Show Slide
    $cmb_hero->add_group_field(
        $hero_id,
        array(
            'name'       => __('Show Slide', 'wedonutscombr'),
            'desc'       => '',
            'id'         => 'show_slide',
            'type'       => 'checkbox',
            'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
        )
    );

    //Background Color
    $cmb_hero->add_group_field(
        $hero_id,
        array(
            'name'       => __('Background Color', 'wedonutscombr'),
            'desc'       => '',
            'id'         => 'bkg_color',
            'type'       => 'colorpicker',
            'default'    => '#ffffff',
            'attributes' => array(
                'data-colorpicker' => json_encode(
                    array(
                        'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                    ),
                ),
            ),
        )
    );

    //Hero Image
    $cmb_hero->add_group_field(
        $hero_id,
        array(
            'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
            'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
            'id'          => 'bkg_image_landscape',
            'type'        => 'file',
            // Optional:
            'options' => array(
                'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                'type' => array(
                //'image/gif',
                    'image/jpeg',
                    'image/png',
                ),
            ),
            'preview_size' => array(280, 59)
        )
    );

    //Hero Background Video
    $cmb_hero->add_group_field(
        $hero_id,
        array(
            'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
            'desc' => __('Format: MP4 (muted) | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
            'id'          => 'bkg_video_landscape',
            'type'        => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
            ),
            'query_args' => array(
                'type' => array(
                    'video/mp4',
                ),
            ),
            'preview_size' => array(280, 59)
        )
    );

    //Hero Image
    $cmb_hero->add_group_field(
        $hero_id,
        array(
            'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
            'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
            'id'          => 'bkg_image_portrait',
            'type'        => 'file',
            // Optional:
            'options' => array(
                'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                'type' => array(
                //'image/gif',
                    'image/jpeg',
                    'image/png',
                ),
            ),
            'preview_size' => array(118, 105)
        )
    );

    //Hero Background Video
    $cmb_hero->add_group_field(
        $hero_id,
        array(
            'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
            'desc' => __('Format: MP4 (muted) | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
            'id'          => 'bkg_video_portrait',
            'type'        => 'file',
            'options' => array(
                'url' => false, 
            ),
            'text'    => array(
                'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
            ),
            'query_args' => array(
                'type' => array(
                    'video/mp4',
                ),
            ),
            'preview_size' => array(118, 105)
        )
    );

    //Hero Background Video Loop
    $cmb_hero->add_group_field(
        $hero_id,
        array(
            'name'       => __('Looping Background Video', 'wedonutscombr'),
            'desc'       => '',
            'id'         => 'bkg_video_loop',
            'type'       => 'checkbox',
        )
    );

    //Hero content
    $cmb_hero->add_group_field(
        $hero_id,
        array(
            'name'    =>  __('HTML Content', 'wedonutscombr'),
            'desc'    => '',
            'id'      => 'content',
            'type'    => 'wysiwyg',
            'options' => array(
                'wpautop' => false,
                'textarea_rows' => get_option('default_post_edit_rows', 10),
            ),
        )
    );

    /******
     * Page Content
     ******/
    $cmb_content = new_cmb2_box(
        array(
            'id'            => $prefix . 'content_id',
            'title'         => __('Page Content', 'wedonutscombr'),
            'object_types'  => $object_types, // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    /**
     * Store Information
    */
    $cmb_store = new_cmb2_box(
        array(
            'id'            => $prefix . 'information_id',
            'title'         => __('Store Information', 'wedonutscombr'),
            'object_types'  => $object_types, // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Name
    $cmb_store->add_field(
        array(
            'name'       => __('Name', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'name',
            'type'       => 'text',
        )
    );

    //Address
    $cmb_store->add_field(
        array(
            'name'       => __('Address', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'address',
            'type'       => 'text',
        )
    );

    //Service Radius
    $cmb_store->add_field(
        array(
            'name'       => __('Service Radius', 'wedonutscombr'),
            'desc'       => __('In kilometers (0 to disable)', 'wedonutscombr'),
            'id'         => $prefix . 'service_radius',
            'type'       => 'text',
            'attributes' => array(
                'type' => 'number',
                'min' => '0',
                'max' => '100',
                'step' => '10',
            ),
            'default' => '0',
        )
    );

    //Phone
    $cmb_store->add_field(
        array(
            'name'       => __('Phone', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'phone',
            'type'       => 'text_medium',
        )
    );

    //Google Maps International Phone Number
    $cmb_store->add_field(
        array(
            'id'         => $prefix . 'phone_number',
            'type' => 'hidden',
        )
    );

    /**
     * Opening Hours
    */
    $cmb_opening = new_cmb2_box(
        array(
            'id'            => $prefix . 'opening_hours_id',
            'title'         => __('Opening hours', 'wedonutscombr'),
            'object_types'  => $object_types, // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    /**
     * Opening Hours - Sunday
    */
    $cmb_opening->add_field(
        array(
        'id'   => $prefix . 'opening_hours_sunday_title',
        'name' => __('Sunday', 'wedonutscombr'),
        'desc' => '',
        'type' => 'title'
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Operation', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'opening_hours_sunday_operation',
            'type'    => 'radio_inline',
            'options' => array(
                'open'    => __('Open', 'wedonutscombr'),
                'close'    => __('Close', 'wedonutscombr'),
            ),
            'default' => 'open',
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Opening Time', 'wedonutscombr'),
            'desc'       => __('E.g. 09:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_sunday_open',
            'type'       => 'text_small',
            'default' => '12:00',
        ),
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Closing Time', 'wedonutscombr'),
            'desc'       => __('E.g. 20:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_sunday_close',
            'type'       => 'text_small',
            'default' => '20:00',
        ),
    );

    /**
     * Opening Hours - Monday
    */
    $cmb_opening->add_field(
        array(
        'id'   => $prefix . 'opening_hours_monday_title',
        'name' => __('Monday', 'wedonutscombr'),
        'desc' => '',
        'type' => 'title'
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Operation', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'opening_hours_monday_operation',
            'type'    => 'radio_inline',
            'options' => array(
                'open'    => __('Open', 'wedonutscombr'),
                'close'    => __('Close', 'wedonutscombr'),
            ),
            'default' => 'open',
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Opening Time', 'wedonutscombr'),
            'desc'       => __('E.g. 09:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_monday_open',
            'type'       => 'text_small',
            'default' => '12:00',
        ),
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Closing Time', 'wedonutscombr'),
            'desc'       => __('E.g. 20:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_monday_close',
            'type'       => 'text_small',
            'default' => '20:00',
        ),
    );

    /**
     * Opening Hours - Tuesday
    */
    $cmb_opening->add_field(
        array(
        'id'   => $prefix . 'opening_hours_tuesday_title',
        'name' => __('Tuesday', 'wedonutscombr'),
        'desc' => '',
        'type' => 'title'
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Operation', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'opening_hours_tuesday_operation',
            'type'    => 'radio_inline',
            'options' => array(
                'open'    => __('Open', 'wedonutscombr'),
                'close'    => __('Close', 'wedonutscombr'),
            ),
            'default' => 'open',
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Opening Time', 'wedonutscombr'),
            'desc'       => __('E.g. 09:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_tuesday_open',
            'type'       => 'text_small',
            'default' => '12:00',
        ),
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Closing Time', 'wedonutscombr'),
            'desc'       => __('E.g. 20:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_tuesday_close',
            'type'       => 'text_small',
            'default' => '20:00',
        ),
    );
    
    /**
     * Opening Hours - Wednesday
    */
    $cmb_opening->add_field(
        array(
        'id'   => $prefix . 'opening_hours_wednesday_title',
        'name' => __('Wednesday', 'wedonutscombr'),
        'desc' => '',
        'type' => 'title'
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Operation', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'opening_hours_wednesday_operation',
            'type'    => 'radio_inline',
            'options' => array(
                'open'    => __('Open', 'wedonutscombr'),
                'close'    => __('Close', 'wedonutscombr'),
            ),
            'default' => 'open',
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Opening Time', 'wedonutscombr'),
            'desc'       => __('E.g. 09:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_wednesday_open',
            'type'       => 'text_small',
            'default' => '12:00',
        ),
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Closing Time', 'wedonutscombr'),
            'desc'       => __('E.g. 20:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_wednesday_close',
            'type'       => 'text_small',
            'default' => '20:00',
        ),
    );
    
    /**
     * Opening Hours - Thursday
    */
    $cmb_opening->add_field(
        array(
        'id'   => $prefix . 'opening_hours_thursday_title',
        'name' => __('Thursday', 'wedonutscombr'),
        'desc' => '',
        'type' => 'title'
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Operation', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'opening_hours_thursday_operation',
            'type'    => 'radio_inline',
            'options' => array(
                'open'    => __('Open', 'wedonutscombr'),
                'close'    => __('Close', 'wedonutscombr'),
            ),
            'default' => 'open',
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Opening Time', 'wedonutscombr'),
            'desc'       => __('E.g. 09:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_thursday_open',
            'type'       => 'text_small',
            'default' => '12:00',
        ),
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Closing Time', 'wedonutscombr'),
            'desc'       => __('E.g. 20:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_thursday_close',
            'type'       => 'text_small',
            'default' => '20:00',
        ),
    );

    /**
     * Opening Hours - Friday
    */
    $cmb_opening->add_field(
        array(
        'id'   => $prefix . 'opening_hours_friday_title',
        'name' => __('Friday', 'wedonutscombr'),
        'desc' => '',
        'type' => 'title'
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Operation', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'opening_hours_friday_operation',
            'type'    => 'radio_inline',
            'options' => array(
                'open'    => __('Open', 'wedonutscombr'),
                'close'    => __('Close', 'wedonutscombr'),
            ),
            'default' => 'open',
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Opening Time', 'wedonutscombr'),
            'desc'       => __('E.g. 09:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_friday_open',
            'type'       => 'text_small',
            'default' => '12:00',
        ),
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Closing Time', 'wedonutscombr'),
            'desc'       => __('E.g. 20:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_friday_close',
            'type'       => 'text_small',
            'default' => '20:00',
        ),
    );

    /**
     * Opening Hours - Saturday
    */
    $cmb_opening->add_field(
        array(
        'id'   => $prefix . 'opening_hours_saturday_title',
        'name' => __('Saturday', 'wedonutscombr'),
        'desc' => '',
        'type' => 'title'
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Operation', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'opening_hours_saturday_operation',
            'type'    => 'radio_inline',
            'options' => array(
                'open'    => __('Open', 'wedonutscombr'),
                'close'    => __('Close', 'wedonutscombr'),
            ),
            'default' => 'open',
        )
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Opening Time', 'wedonutscombr'),
            'desc'       => __('E.g. 09:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_saturday_open',
            'type'       => 'text_small',
            'default' => '12:00',
        ),
    );

    $cmb_opening->add_field(
        array (
            'name'       => __('Closing Time', 'wedonutscombr'),
            'desc'       => __('E.g. 20:00', 'wedonutscombr'),
            'id'         => $prefix . 'opening_hours_saturday_close',
            'type'       => 'text_small',
            'default' => '20:00',
        ),
    );

    /**
     * Orders
    */
    $cmb_orders = new_cmb2_box(
        array(
            'id'            => '_wedonuts_store_orders_id',
            'title'         => __('Orders', 'wedonutscombr'),
            'object_types'  => $object_types, // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //WhatsApp
    $cmb_orders->add_field(
        array(
            'name'       => __('WhatsApp (URL)', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'orders_whatsapp',
            'type'       => 'text_url',
        )
    );

    //iFood
    $cmb_orders->add_field(
        array(
            'name'       => __('iFood (URL)', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'orders_ifood',
            'type'       => 'text_url',
        )
    );

    //Goomer
    $cmb_orders->add_field(
        array(
            'name'       => __('Goomer (URL)', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'orders_goomer',
            'type'       => 'text_url',
        )
    );

    //Cardapin
    $cmb_orders->add_field(
        array(
            'name'       => __('Cardapin (URL)', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'orders_cardapin',
            'type'       => 'text_url',
        )
    );

    //Menume
    $cmb_orders->add_field(
        array(
            'name'       => __('Menume (URL)', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'orders_menume',
            'type'       => 'text_url',
        )
    );
    
    //Anota AI
    $cmb_orders->add_field(
        array(
            'name'       => __('Anota AI (URL)', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'orders_anotaai',
            'type'       => 'text_url',
        )
    );

    /**
     * Google Maps
    */
    $cmb_maps = new_cmb2_box(
        array(
            'id'            => '_wedonuts_store_maps_id',
            'title'         => __('Google Maps', 'wedonutscombr'),
            'object_types'  => $object_types, // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, 
        )
    );

    //Google Maps Place ID
    $cmb_maps->add_field(
        array(
            'name'       => __('Google Maps Place ID', 'wedonutscombr'),
            'desc'       => __('Place ID Finder: https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder', 'wedonutscombr'),
            'id'         => $prefix . 'place_id',
            'type'       => 'text',
        )
    );

    //Google Maps Show
    $cmb_maps->add_field(
        array(
            'name'       => __('Show Map', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'show_map',
            'type'       => 'checkbox',
            'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
        )
    );

    //Google Maps iframe
    $cmb_maps->add_field(
        array(
            'name'       => __('Google Maps iframe', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'maps_iframe',
            'type'       => 'textarea_code',
        )
    );
    
    //Google Maps Routes Show
    $cmb_maps->add_field(
        array(
            'name'       => __('Show Routes Button', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'show_map_routes',
            'type'       => 'checkbox',
            'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
        )
    );

    //Google Maps Routes Link
    $cmb_maps->add_field(
        array(
            'name'       => __('Google Maps Routes Link (URL)', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'maps_routes_url',
            'type'       => 'text_url',
        )
    );

    //Google Maps Update
    $cmb_maps->add_field(
        array(
            'name'       => __('Update data from Google Maps', 'wedonutscombr'),
            'desc'       => __('Automatically update data via Google Maps', 'wedonutscombr'),
            'id'         => $prefix . 'update_from_maps',
            'type'       => 'checkbox',
            'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
        )
    );

    //Google Maps Update Interval
    $cmb_maps->add_field(
        array(
            'name'       => __('Update interval', 'wedonutscombr'),
            'desc'       => '',
            'id'         => $prefix . 'maps_update_interval',
            'type'    => 'radio',
            'options' => array(
                '1'   => __('1 hour', 'wedonutscombr'),
                '6'   => __('6 hours', 'wedonutscombr'),
                '12'  => __('12 hours', 'wedonutscombr'),
                '24'  => __('24 hours', 'wedonutscombr'),
            ),
            'default' => '24',
        )
    );

    //Google Maps Last Update
    $cmb_maps->add_field(
        array(
            'id'         => $prefix . 'maps_last_update',
            'type' => 'hidden',
        )
    );

    //Google Maps Geometry Location Lat
    $cmb_maps->add_field(
        array(
            'id'         => $prefix . 'origem_location_lat',
            'type' => 'hidden',
        )
    );

    //Google Maps Geometry Location Lng
    $cmb_maps->add_field(
        array(
            'id'         => $prefix . 'origem_location_lng',
            'type' => 'hidden',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_wedonuts_store');

/***********************************************************************************
 * Pages Custom Fields
 * ********************************************************************************/

/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_wedonutscombr_frontpage_';
        $show_on = 'Show_On_Front_page';

        /**
         * Announcement Bar
         */
        $cmb_announcement_bar = new_cmb2_box(
            array(
                'id'            => $prefix . 'announcement_bar_id',
                'title'         => __('Announcement Bar', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
        
        //Announcement Bar Show
        $cmb_announcement_bar->add_field(
            array(
                'name'       => __('Show Announcement Bar', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_announcement_bar',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Announcement Bar Autoplay
        $cmb_announcement_bar->add_field(
            array(
                'name'       => __('Autoplay Announcement Bar', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'announcement_bar_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );
        
        //Announcement Bar Group
        $announcement_bar_id = $cmb_announcement_bar->add_field(
            array(
                'id'          => $prefix . 'announcement_bar_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Announcement Bar Show Slide
        $cmb_announcement_bar->add_group_field(
            $announcement_bar_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Background Color
        $cmb_announcement_bar->add_group_field(
            $announcement_bar_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Announcement Bar content
        $cmb_announcement_bar->add_group_field(
            $announcement_bar_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    //'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 1),
                    'media_buttons' => false,
                    'teeny' => true,
                ),
            )
        );

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
        
        //Hero Show
        $cmb_hero->add_field(
            array(
                'name'       => __('Show Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_hero',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );
        
        //Hero Autoplay
        $cmb_hero->add_field(
            array(
                'name'       => __('Autoplay Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x1080px | Aspect ratio: 64:27', 'wedonutscombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x1080px | Aspect ratio: 64:27', 'wedonutscombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x1920px | Aspect ratio: 9:16', 'wedonutscombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 210)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x1920px | Aspect ratio: 9:16', 'wedonutscombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero content
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 10),
                ),
            )
        );

        //Hero Target
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Target', 'wedonutscombr'),
                'desc'       => __('Select target', 'wedonutscombr'),
                'id'         => 'target',
                'type'       => 'radio',
                'show_option_none' => false,
                'options'          => array(
                    'none'   => __('None', 'wedonutscombr'),
                    'page'   => __('Page', 'wedonutscombr'),
                    'link'  => __('Link (URL)', 'wedonutscombr'),
                ),
                'default' => 'none',
            )
        );

        //Hero Target Page
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Page Target', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'target_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );

        //Hero Link
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Link (URL)', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'target_link_url',
                'type'       => 'text',
            )
        );

        /******
         * Page Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => $prefix . 'content_id',
                'title'         => __('Page Content', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on_cb'    => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //HTML Content
        $cmb_content->add_field(
            array(
                'name'       => __('HTML Content', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 30)
                ),
            )
        );
    }
);
    
/**
 * Promotions
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_wedonutscombr_promo_';
        $show_on = array('key' => 'page-template', 'value' => 'page-promocoes.blade.php');

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
           
        //Hero Show
        $cmb_hero->add_field(
            array(
                'name'       => __('Show Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_hero',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Autoplay
        $cmb_hero->add_field(
            array(
                'name'       => __('Autoplay Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x1080px | Aspect ratio: 64:27', 'wedonutscombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x1080px | Aspect ratio: 64:27', 'wedonutscombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x1920px | Aspect ratio: 9:16', 'wedonutscombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 210)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x1920px | Aspect ratio: 9:16', 'wedonutscombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero content
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 10),
                ),
            )
        );

        //Hero Target
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Target', 'wedonutscombr'),
                'desc'       => __('Select target', 'wedonutscombr'),
                'id'         => 'target',
                'type'       => 'radio',
                'show_option_none' => false,
                'options'          => array(
                    'none'   => __('None', 'wedonutscombr'),
                    'page'   => __('Page', 'wedonutscombr'),
                    'link'  => __('Link (URL)', 'wedonutscombr'),
                ),
                'default' => 'none',
            )
        );

        //Hero Target Page
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Page Target', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'target_page_id',
                'type'       => 'select',
                'options'    => Get_Post_array(array('post_type' => 'page'), true),
            )
        );

        //Hero Link
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Link (URL)', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'target_link_url',
                'type'       => 'text',
            )
        );

        /******
         * Page Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => $prefix . 'content_id',
                'title'         => __('Page Content', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //HTML Content
        $cmb_content->add_field(
            array(
                'name'       => __('HTML Content', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 30)
                ),
            )
        );
    }
);

/**
 * Promo Single
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_wedonutscombr_promo-single_';
        $show_on = array('key' => 'page-template', 'value' => 'page-promocoes-single.blade.php');

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
            
        //Hero Show
        $cmb_hero->add_field(
            array(
                'name'       => __('Show Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_hero',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Autoplay
        $cmb_hero->add_field(
            array(
                'name'       => __('Autoplay Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );
        
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );
    
        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );
    
        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero content
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 10),
                ),
            )
        );

        /******
         * Page Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => $prefix . 'content_id',
                'title'         => __('Page Content', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //HTML Content
        $cmb_content->add_field(
            array(
                'name'       => __('HTML Content', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 30)
                ),
            )
        );
    }
);

/**
 * Menu
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_wedonutscombr_menu_';
        $show_on = array('key' => 'page-template', 'value' => 'page-cardapio.blade.php');

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Show
        $cmb_hero->add_field(
            array(
                'name'       => __('Show Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_hero',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Autoplay
        $cmb_hero->add_field(
            array(
                'name'       => __('Autoplay Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );
        
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );
        
        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x1080px | Aspect ratio: 64:27', 'wedonutscombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x1080px | Aspect ratio: 64:27', 'wedonutscombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x1920px | Aspect ratio: 9:16', 'wedonutscombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 210)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x1920px | Aspect ratio: 9:16', 'wedonutscombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero content
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 10),
                ),
            )
        );

        /******
         * Page Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => $prefix . 'content_id',
                'title'         => __('Page Content', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //HTML Content
        $cmb_content->add_field(
            array(
                'name'       => __('HTML Content', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 30)
                ),
            )
        );
    }
);

/**
 * Menu Single
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_wedonutscombr_menu-single_';
        $show_on = array('key' => 'page-template', 'value' => 'page-cardapio-single.blade.php');

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Show
        $cmb_hero->add_field(
            array(
                'name'       => __('Show Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_hero',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Autoplay
        $cmb_hero->add_field(
            array(
                'name'       => __('Autoplay Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );
        
        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x1080px | Aspect ratio: 64:27', 'wedonutscombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x1080px | Aspect ratio: 64:27', 'wedonutscombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x1920px | Aspect ratio: 9:16', 'wedonutscombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 210)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x1920px | Aspect ratio: 9:16', 'wedonutscombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 118)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero content
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 10),
                ),
            )
        );

        /******
         * Page Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => $prefix . 'content_id',
                'title'         => __('Page Content', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //HTML Content
        $cmb_content->add_field(
            array(
                'name'       => __('HTML Content', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 30)
                ),
            )
        );
    }
);

/**
 * Stores
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_wedonutscombr_stores_';
        $show_on = array('key' => 'page-template', 'value' => 'page-unidades.blade.php');

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Show
        $cmb_hero->add_field(
            array(
                'name'       => __('Show Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_hero',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Autoplay
        $cmb_hero->add_field(
            array(
                'name'       => __('Autoplay Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );
        
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero content
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 10),
                ),
            )
        );

        /******
         * Page Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => $prefix . 'content_id',
                'title'         => __('Page Content', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //HTML Content
        $cmb_content->add_field(
            array(
                'name'       => __('HTML Content', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 30)
                ),
            )
        );
    }
);

/**
 * Stores Single
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_wedonutscombr_stores-single_';
        $show_on = array('key' => 'page-template', 'value' => 'page-unidades-single.blade.php');

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Show
        $cmb_hero->add_field(
            array(
                'name'       => __('Show Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_hero',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Autoplay
        $cmb_hero->add_field(
            array(
                'name'       => __('Autoplay Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero content
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 10),
                ),
            )
        );

        /******
         * Page Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => $prefix . 'content_id',
                'title'         => __('Page Content', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //HTML Content
        $cmb_content->add_field(
            array(
                'name'       => __('HTML Content', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 30)
                ),
            )
        );
    }
);

/**
 * About Us
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_wedonutscombr_aboutus_';
        $show_on = array('key' => 'page-template', 'value' => 'page-quem-somos.blade.php');

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'aboutus_hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Show
        $cmb_hero->add_field(
            array(
                'name'       => __('Show Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_hero',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Autoplay
        $cmb_hero->add_field(
            array(
                'name'       => __('Autoplay Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero content
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 10),
                ),
            )
        );

        /******
         * Page Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => $prefix . 'content_id',
                'title'         => __('Page Content', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //HTML Content
        $cmb_content->add_field(
            array(
                'name'       => __('HTML Content', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 30)
                ),
            )
        );
    }
);

/**
 * Franchises
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_wedonutscombr_franchises_';
        $show_on = array('key' => 'page-template', 'value' => 'page-franquias.blade.php');

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Show
        $cmb_hero->add_field(
            array(
                'name'       => __('Show Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_hero',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Autoplay
        $cmb_hero->add_field(
            array(
                'name'       => __('Autoplay Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero content
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 10),
                ),
            )
        );

        /******
         * Page Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => $prefix . 'content_id',
                'title'         => __('Page Content', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //HTML Content
        $cmb_content->add_field(
            array(
                'name'       => __('HTML Content', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 30)
                ),
            )
        );
    }
);

/**
 * Resellers
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_wedonutscombr_resellers_';
        $show_on = array('key' => 'page-template', 'value' => 'page-revendedores.blade.php');

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Show
        $cmb_hero->add_field(
            array(
                'name'       => __('Show Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_hero',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Autoplay
        $cmb_hero->add_field(
            array(
                'name'       => __('Autoplay Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero content
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 10),
                ),
            )
        );

        /******
         * Page Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => $prefix . 'content_id',
                'title'         => __('Page Content', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //HTML Content
        $cmb_content->add_field(
            array(
                'name'       => __('HTML Content', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 30)
                ),
            )
        );
    }
);

/**
 * Jobs
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_wedonutscombr_jobs_';
        $show_on = array('key' => 'page-template', 'value' => 'page-trabalhe-conosco.blade.php');

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Show
        $cmb_hero->add_field(
            array(
                'name'       => __('Show Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_hero',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Autoplay
        $cmb_hero->add_field(
            array(
                'name'       => __('Autoplay Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero content
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 10),
                ),
            )
        );

        /******
         * Page Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => $prefix . 'content_id',
                'title'         => __('Page Content', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //HTML Content
        $cmb_content->add_field(
            array(
                'name'       => __('HTML Content', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 30)
                ),
            )
        );
    }
);

/**
 * Contact
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_wedonutscombr_contact_';
        $show_on = array('key' => 'page-template', 'value' => 'page-contato.blade.php');

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => $prefix . 'hero_id',
                'title'         => __('Hero', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Show
        $cmb_hero->add_field(
            array(
                'name'       => __('Show Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'show_hero',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Hero Autoplay
        $cmb_hero->add_field(
            array(
                'name'       => __('Autoplay Hero', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_autoplay',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );
        
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'desc' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'wedonutscombr'),
                    'add_button'   =>__('Add Another Slide', 'wedonutscombr'),
                    'remove_button' =>__('Remove Slide', 'wedonutscombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Hero Show Slide
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Show Slide', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'show_slide',
                'type'       => 'checkbox',
                'default_cb' => 'cmb2_set_checkbox_default_for_new_post',
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff','#f95fb0','#eb4296','#d10f82','#fcc86b','#fbb833','#f79e00','#efecea','#f0e2d7','#e2cec0','#f76341','#ed4f24','#d63a16','#753627','#592212','#42150a','#5991ce','#3e73b5','#235891','#f9f9f9','#ebebeb','#d1d1d1','#6b6b6b','#333333','#0a0a0a'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Landscape)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 2560x540px | Aspect ratio: 128:27', 'wedonutscombr'),
                'id'          => 'bkg_video_landscape',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(280, 59)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'wedonutscombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'wedonutscombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Video (Portrait)', 'wedonutscombr'),
                'desc' => __('Format: MP4 (muted) | Recommended size: 1080x960px | Aspect ratio: 9:8', 'wedonutscombr'),
                'id'          => 'bkg_video_portrait',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Video', 'wedonutscombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'video/mp4',
                    ),
                ),
                'preview_size' => array(118, 105)
            )
        );

        //Hero Background Video Loop
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Looping Background Video', 'wedonutscombr'),
                'desc'       => '',
                'id'         => 'bkg_video_loop',
                'type'       => 'checkbox',
            )
        );

        //Hero content
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'    =>  __('HTML Content', 'wedonutscombr'),
                'desc'    => '',
                'id'      => 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 10),
                ),
            )
        );

        /******
         * Page Content
         ******/
        $cmb_content = new_cmb2_box(
            array(
                'id'            => $prefix . 'content_id',
                'title'         => __('Page Content', 'wedonutscombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => $show_on,
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //HTML Content
        $cmb_content->add_field(
            array(
                'name'       => __('HTML Content', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'content',
                'type'    => 'wysiwyg',
                'options' => array(
                    'wpautop' => false,
                    'textarea_rows' => get_option('default_post_edit_rows', 30)
                ),
            )
        );

        /**
        * Social Networks
        */
        $cmb_social = new_cmb2_box(
            array(
                'id'            => $prefix . 'social_id',
                'title'         => __('Social Networks', 'wedonutscombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on'       => $show_on,
            )
        );

        //Facebook (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('Facebook (URL)', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'facebook_url',
                'type'       => 'text_url',
            )
        );

        //Instagram (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('Instagram (URL)', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'instagram_url',
                'type'       => 'text_url',
            )
        );

        //TikTok (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('TikTok (URL)', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'tiktok_url',
                'type'       => 'text_url',
            )
        );

        //X (Twitter) (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('X (Twitter) (URL)', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'twitter_url',
                'type'       => 'text_url',
            )
        );

        //LinkedIn (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('LinkedIn (URL)', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'linkedin_url',
                'type'       => 'text_url',
            )
        );

        //Youtube (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('Youtube (URL)', 'wedonutscombr'),
                'desc'       => '',
                'id'         => $prefix . 'youtube_url',
                'type'       => 'text_url',
            )
        );
    }
);
