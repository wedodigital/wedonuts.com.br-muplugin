��    n      �  �   �      P	     Q	     X	     a	     j	     r	  	   �	     �	     �	  	   �	     �	  
   �	     �	     �	  )   �	     
     %
     3
     D
     a
     }
     �
     �
     �
     �
  
   �
  
   �
  
   �
     �
            S   )  Q   }  T   �  T   $  H   y  F   �  I   	  I   S     �     �     �     �     �     �                    2     C  
   S     ^     m     �     �     �  	   �     �  	   �     �     �     �     �  	   �     �     �                    (  k   .     �     �     �     �     �     �     �       	        #     ,  
   ?  	   J     T     d     j     y     �     �     �     �     �     �     �     �     �     �     �          ,     B  
   X     c     o  	   �     �     �     �     �  f  �     ,     3     <     E     M     b     s     �     �  	   �     �     �     �  3   �          5     C     P     k     �     �     �     �     �  	   �  
   �     �     	          +  V   B  T   �  W   �  W   F  P   �  Q   �  Q   A  Q   �     �     �     �  !        7     M     Z     i  $   n     �     �  
   �     �     �     �     �       	                  )     D     K     `     z     �     �     �     �  	   �     �  s   �     A     O     j     r     �     �     �     �     �     �  "   �       	   '     1     ?     D     U     h     ~     �     �     �     �     �     �     �     �      �          0     G     b     k     x     �     �     �     �     �     1   X      l       P   -   0           Z           V      ^           9   m   +           d   &   E   ]   e   b          C       a   f          8   5   %   4   H   O   2   g          R   D   G   L       3       *      `               M   .       	       _       !   j      N   $      S   W       ;                "      h      A   I   B   c       7   @   ?      K   i                     Q   ,   J   
   '   (          U   k           #   :   6       /   )       =   >                 T      <      Y          n   [            F   \                                 1 hour 12 hours 24 hours 6 hours Add Another Slide Add Image Add New Add New Store Add Video Address All Stores Announcement Bar Anota AI (URL) Automatically update data via Google Maps Autoplay Announcement Bar Autoplay Hero Background Color Background Image (Landscape) Background Image (Portrait) Background Video (Landscape) Background Video (Portrait) Cardapin (URL) Close Closing Time E.g. 09:00 E.g. 20:00 Edit Store Facebook (URL) Featured Image Filter cases list Format: JPG (recommended), PNG | Recommended size: 1080x1920px | Aspect ratio: 9:16 Format: JPG (recommended), PNG | Recommended size: 1080x960px | Aspect ratio: 9:8 Format: JPG (recommended), PNG | Recommended size: 2560x1080px | Aspect ratio: 64:27 Format: JPG (recommended), PNG | Recommended size: 2560x540px | Aspect ratio: 128:27 Format: MP4 (muted) | Recommended size: 1080x1920px | Aspect ratio: 9:16 Format: MP4 (muted) | Recommended size: 1080x960px | Aspect ratio: 9:8 Format: MP4 (muted) | Recommended size: 2560x1080px | Aspect ratio: 64:27 Format: MP4 (muted) | Recommended size: 2560x540px | Aspect ratio: 128:27 Friday Google Maps Google Maps Place ID Google Maps Routes Link (URL) Google Maps iframe Goomer (URL) HTML Content Hero In kilometers (0 to disable) Insert into case Instagram (URL) Link (URL) LinkedIn (URL) Looping Background Video Menume (URL) Monday Name New Store None Not found Not found in Trash Open Opening Time Opening hours Operation Orders Page Page Content Page Target Parent Store: Phone Place ID Finder: https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder Remove Slide Remove featured image Saturday Search Store Select target Service Radius Set featured image Show Announcement Bar Show Hero Show Map Show Routes Button Show Slide Slide {#} Social Networks Store Store Archives Store Attributes Store Information Stores Stores list Stores list navigation Sunday Target Thursday TikTok (URL) Tuesday Update Store Update data from Google Maps Update interval Uploaded to this case Use as featured image View Store View Stores We Donuts Stores Wednesday WhatsApp (URL) X (Twitter) (URL) Youtube (URL) iFood (URL) Project-Id-Version: 
PO-Revision-Date: 2023-09-19 20:01-0300
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.3.2
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: wedonutscombr.php
 1 hora 12 horas 24 horas 6 horas Adicionar Novo Slide Adicionar imagem Adicionar Novo(a) Adicionar Nova Loja Adicionar Vídeo Endereço Todas as Lojas Barra de Anúncios Anota AI (URL) Atualizar automaticamente os dados pelo Google Maps Barra de Anúncios Autoplay Hero Autoplay Cor de fundo Imagem de fundo (Paisagem) Imagem de fundo (Retrato) Vídeo de fundo (Paisagem) Vídeo de fundo (Retrato) Cardapin (URL) Fechada Horário de Fechamento Ex. 09:00 Ex.: 20:00 Editar Loja Facebook (URL) Imagem em destaque Filtrar lista de Lojas Formato: JPG (recomendado), PNG | Tamanho recomendado: 1080x1920px | Proporção: 9:16 Formato: JPG (recomendado), PNG | Tamanho recomendado: 1080x960px | Proporção: 9:8 Formato: JPG (recomendado), PNG | Tamanho recomendado: 2560x1080px | Proporção: 64:27 Formato: JPG (recomendado), PNG | Tamanho recomendado: 2560x540px | Proporção: 128:27 Formato: MP4 (silenciado) | Tamanho recomendado: 1080x1920px | Proporção: 9:16 Formato: MP4 (silenciado) | Tamanho recomendado: 1080x960px | Proporção: 128:27 Formato: MP4 (silenciado) | Tamanho recomendado: 2560x1080px | Proporção: 64:27 Formato: MP4 (silenciado) | Tamanho recomendado: 2560x540px | Proporção: 128:27 Sexta-feira Google Maps Place ID do Google Maps Link da Rota do Google Maps (URL) Iframe do Google Maps Goomer (URL) Conteúdo HTML Hero Em quilômetros (0 para desabilitar) Inserir em case Instagram (URL) Link (URL) LinkedIn (URL) Vídeo de fundo em looping Menume (URL) Segunda-feira Nome Nova Loja Nenhum Não encontrado Não encontrado na lixeira Aberta Horário de Abertura Horário de funcionamento Funcionamento Pedidos Página Conteúdo da Página Página Alvo Loja Pai: Telefone Localizador do Place ID: https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder Remover Slide Remover imagem em destaque Sábado Pesquisar Loja Selecione o alvo Raio de Atendimento Definir imagem em destaque Mostrar Barra de Anúncios Mostrar Hero Mostrar mapa Mostrar botão Rotas (Como chegar) Mostrar Slide Slide {#} Redes Sociais Loja Arquivo de Lojas Atributos de lojas Informações da Loja Lojas Lista de Lojas Navegação na lista de Lojas Domingo Alvo Quinta-feira TikTok (URL) Terça-feira Atualizar Loja Atualizar dados pelo Google Maps Intervalo de atualização Enviado para este case Usar como imagem destacada Ver Loja Ver as Lojas Lojas da We Donuts Quarta-feira WhatsApp (URL) X (Twitter) (URL) Youtube (URL) iFood (URL) 